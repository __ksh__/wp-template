# README #

### setup ###
```
git clone git@bitbucket.org:__ksh__/wp-template.git && cd wp-template
chmod -R 700 bin
chmod -R 777 db
docker-machine start && eval $(docker-machine env)
docker-compose build && docker-compose up -d
```

### tips ###

** how to use wp-cli **
```
docker exec -it {container name} bin/wp --allow-root [options]
```